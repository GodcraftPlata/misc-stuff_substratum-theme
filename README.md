# misc-stuff_substratum-theme
Hello there! I'm glad to say that I got as far as I wanted with this project. More features will be added, of course. Enjoy!

Thanks to the substratum team for the template (https://github.com/substratum/template), pierx for the inspiration to make a little splash screen (even if it isn't working, like at all) and everyone who helped me (especially djdarkknight96, who taught me the basics)

As for now, we got the following:

• WhatsApp overlay that changes the emojis within the app (only Unicode emojis option tho)

• Hello Moto bootanimation

• iOS Black bootanimation

• iOS Keyboard sounds (not working within substratum)

• iOS Lock-Unlock sounds

• Motorola old sounds

• Motorola new sounds

• Hide "Clear All" button on Lawnchair, Quickstep and OneUI launcher

• iOS Recents on Lawnchair (broken)

Another thing to note, I use master for everything. If something breaks there, I'll just fix it.

Lastly, if bootanimations or sound packs aren't being applied, go to the following link to apply them manually:

https://drive.google.com/open?id=1n_TEWBE1ZHpfbSeGFCiFb-Y8PBkyHiZD
